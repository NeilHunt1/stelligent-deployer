##Stelligent Mini-Project
##Note that Bitbucket Markdown formatting currently not working properly!
###About
Project to support Stelligent interview process. Basic functional steps:
1.  Reads in required parameters (see below)
2.  Creates a Amazon Linux EC2 instance in the VPC associated with the subnet you specify with the specified keyfile associated with the ec2-user user
3.  Assigns a public IP address to the instance
4.  SSH's into the newly created instance and uploads HTML content to the server
5.  Tests that the web server is available and that the content being served is identical to the original content
###Required properties
These can be specified in src/main/resources/runtime.properties or can be overridden using standard java VM system properties (-D args).
The runtime.properties file in the source directory is for references only and is included in .gitignore from here on out. It's suggested to put
in your own values of AWS credentials, your own VPC (subnet, public IP, security group) and key (keyname in AWS and local key file location)
*  **amiID**: ID of the AMI you want to use, should not have to change from example runtime.properties value
*  **instanceType**: the instance type you want to create. t2.micro, the least expensive, is fine for this exercise, note that this requires use of VPC
*  **subnetID**: ID of the subnet for your VPC
*  **publicIP**: the IP address of the VPC elastic IP you want to map to your web server
*  **associationID**: ID of the VPC elastic IP association
*  **keyName**: filename of your rsa/dsa key name in AWS
*  **secGrp**: group ID of the VPC security group to be used for your instance. Must have at minimum Inbound rules for SSH (port 22) and HTTP (port 80)
*  **privateKeyLocation**: File system path of the local private key to be used during execution of this program (e.g. /home/user/.ssh/id_rsa)
*  **aws.accessKeyId/aws.secretKey**: the AWS keys to be used for environment setup. Must have EC2 create instance permission. **Note that
these values are overridden by values in your ~/.aws/credentials directory if those are specified**
###Execution steps
To execute the program, you must have Apache maven setup (3.2.1 is tested version). Once setup, to execute entire program, run
```
mvn run:exec
```
Typical execution time is <2 minutes. Upon completion, you can access the webserver at http://{publicIP}/stelligent