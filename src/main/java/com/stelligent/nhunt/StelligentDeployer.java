package com.stelligent.nhunt;

import static net.sf.expectit.matcher.Matchers.anyOf;
import static net.sf.expectit.matcher.Matchers.contains;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import net.sf.expectit.Expect;
import net.sf.expectit.ExpectBuilder;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AssociateAddressRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

//TODO: params -  keyname, subnetID, elasticIP allocation ID, user

public class StelligentDeployer {
	
	static Logger LOGGER = LoggerFactory.getLogger(StelligentDeployer.class);
	static String PUBLIC_IP;
	static String INSTANCE_TYPE;
	static String AMI_ID;
	static String SUBNET_ID;
	static String KEY_NAME;
	static String SEC_GRP_ID;
	static String ASSOCIATION_ID;
	static String PRIVATE_KEY_LOCATION;
	
	public static void main(String[] args) {
		
		Properties prop = new Properties();
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("runtime.properties");
		try {
			prop.load(stream);
		} catch (IOException e1) {
			LOGGER.warn("no runtime.properties file specified. All properties must be specified as VM args");
		}
		PUBLIC_IP = readProperty(prop,"publicIP");
		INSTANCE_TYPE = readProperty(prop,"instanceType");
		AMI_ID = readProperty(prop,"amiID");
		SUBNET_ID = readProperty(prop,"subnetID");
		KEY_NAME = readProperty(prop,"keyName");
		SEC_GRP_ID = readProperty(prop,"secGrpID");
		ASSOCIATION_ID = readProperty(prop,"associationID");
		PRIVATE_KEY_LOCATION = readProperty(prop,"privateKeyLocation");
		
		final AmazonEC2Client amazonEC2Client = new AmazonEC2Client();
		RunInstancesRequest runHttpServer = new RunInstancesRequest();
		runHttpServer.setImageId(AMI_ID);
		runHttpServer.setInstanceType(INSTANCE_TYPE);
		runHttpServer.setMinCount(1);
		runHttpServer.setMaxCount(1);
		runHttpServer.setKeyName(KEY_NAME);
		runHttpServer.setSubnetId(SUBNET_ID);
		HashSet<String> secgrps = new HashSet<String>();
		secgrps.add(SEC_GRP_ID);
		runHttpServer.setSecurityGroupIds(secgrps);
		RunInstancesResult runInstancesResult = amazonEC2Client.runInstances(runHttpServer);
		List<Instance> thisInstanceList = runInstancesResult.getReservation().getInstances();
		final String newInstanceId = thisInstanceList.iterator().next().getInstanceId();
		LOGGER.debug("our instance ID is "+newInstanceId);
		
		//check every few seconds when the instance is ready so that we can connect to it
		final Timer timer = new Timer();
		final int timerCheckSeconds = 5;
		timer.schedule(new TimerTask() {
			int time = 0;
			boolean publicIpAssigned = false;
			@Override
			public void run() {
				//need to query/retrieve the instance each time in order to obtain latest status/info
				DescribeInstancesRequest dir = new DescribeInstancesRequest();
				Set<String> instanceList = new HashSet<String>();
				instanceList.add(newInstanceId);
				dir.setInstanceIds(instanceList);
				DescribeInstancesResult instanceResult = amazonEC2Client.describeInstances(dir);
				List<Reservation> runInstancesResult = instanceResult.getReservations();
				//assuming there is only one instance, since we only queried one instanceID
				List<Instance> thisInstanceList = runInstancesResult.get(0).getInstances();
				final Instance myInstance = thisInstanceList.get(0);
				LOGGER.info("at "+time+" seconds, instance state for "+myInstance.getInstanceId()+" is "+myInstance.getState().getName());
				if(myInstance.getState().getName().equals("running")){
					//associate elastic IP address for public connection
					if(!publicIpAssigned){
						LOGGER.debug("associating our public IP address "+PUBLIC_IP+" with our instance "+myInstance.getInstanceId());
						AssociateAddressRequest aar = new AssociateAddressRequest();
						aar.setAllocationId(ASSOCIATION_ID);
						aar.setAllowReassociation(true);
						aar.setInstanceId(myInstance.getInstanceId());
						amazonEC2Client.associateAddress(aar);
						if (myInstance.getPublicIpAddress()==null || !myInstance.getPublicIpAddress().equals(PUBLIC_IP)) {
							time = time+timerCheckSeconds;
							return;
						} else {
							publicIpAssigned=true;
							time = time+timerCheckSeconds;
							return;
						} 
					}
					LOGGER.debug("verifying SSH connectivity");
					JSch jsch = new JSch();
					try {
						jsch.addIdentity(PRIVATE_KEY_LOCATION);
						Session session = jsch.getSession("ec2-user", PUBLIC_IP);
						session.setConfig("StrictHostKeyChecking", "no");
						session.connect();
						LOGGER.info("SSH connection established");
						Channel shellChannel=session.openChannel("shell");
						Expect expect = new ExpectBuilder()
								.withOutput(shellChannel.getOutputStream())
								.withInputs(shellChannel.getInputStream(), shellChannel.getExtInputStream())
								.withEchoInput(System.err)
								.withExceptionOnFailure()
								.build();
						shellChannel.connect();
						expect.sendLine("ifconfig");
						try{
							//simple check to make sure we're connected to the correct instance
							expect.withTimeout(5, TimeUnit.SECONDS).expect(contains(myInstance.getPrivateIpAddress()));
						} catch(IOException e){
							LOGGER.warn("possible that the public IP address hasn't been associated with correct underlying instance");
							return;
						}
						timer.cancel();
						expect.expect(contains("$"));
						expect.sendLine("sudo yum -y update"); //ensure we have latest package versions
						expect.withTimeout(100, TimeUnit.SECONDS).expect(anyOf(contains("Complete!"),contains("No packages marked for update")));
						expect.sendLine("sudo yum -y install httpd");
						expect.expect(anyOf(contains("Complete!"),contains("Nothing to do")));
						expect.sendLine("sudo service httpd start");
						expect.expect(contains("Starting httpd:"));
						expect.sendLine("sudo chown ec2-user /var/www/html");
						expect.expect(contains("$"));
						expect.sendLine("exit");
						ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
						sftpChannel.connect();
						sftpChannel.cd("/var/www/html");
						sftpChannel.mkdir("stelligent");
						sftpChannel.cd("stelligent");
						File indexFile = new File("src/main/web/index.html");
						sftpChannel.put(new FileInputStream(indexFile), indexFile.getName());
						//done, now verify that content is uploaded where expected and it's the correct file(s)
						try {
							String apacheContent = Jsoup.connect("http://"+PUBLIC_IP+"/stelligent").get().html();
							String localContent = Jsoup.parse(indexFile, "UTF-8").html();
							LOGGER.debug("APACHE CONTENT");
							LOGGER.debug(apacheContent);
							LOGGER.debug("LOCAL CONTENT");
							LOGGER.debug(localContent);
							if(apacheContent.trim().equals(localContent.trim())){
								LOGGER.info("Apache server ready with index.html file");
							} else{
								LOGGER.error("The file cannot be retrieved at this time");
								System.exit(0);
							}
						} catch (IOException e) {
							LOGGER.error("The file cannot be retrieved at this time due to: "+e.getMessage());
							System.exit(0);
						}
						LOGGER.debug("disconnecting, exiting");
						sftpChannel.disconnect();
						shellChannel.disconnect();
						session.disconnect();
						System.exit(0);
					} catch (JSchException e) {
						LOGGER.warn("cannot connect to instance due to "+e.getMessage()); //could do more robust checking here for more situations....
						time = time+timerCheckSeconds;
						if (time<120) {
							return;
						} else {
							System.exit(0);
						}
					} catch (IOException e) {
						LOGGER.error("cannot access file due to "+e.getMessage());
					} catch (SftpException e) {
						LOGGER.error("cannot upload due to "+e.getMessage());
						System.exit(0);
					}
				}
				time = time+timerCheckSeconds;
				
			}
		}, 0, timerCheckSeconds*1000);
	}
	/*
	 * method to populate required variables, giving priority to properties defined as runtime variables,
	 * secondary priority to those defined in runtime.properties or failing if undefined
	 * @param propertiesFromFile is pre-populated properties object from runtime.properties, read in earlier
	 * @param propertyName is property we're looking for
	 */
	private static String readProperty(Properties propertiesFromFile, String propertyName){
		if (!(System.getProperty(propertyName)==null)) {
			return System.getProperty(propertyName);
		} else if (!(propertiesFromFile.getProperty(propertyName)==null)) {
			return propertiesFromFile.getProperty(propertyName);
		} else {
			LOGGER.error("critical property undefined: "+propertyName);
			System.exit(0);
			return null;
		}
	}
}
